#![cfg_attr(not(feature = "std"), no_std)]

use ink_lang as ink;

pub use erc20::{Erc20, Transferred}; // re-export to make foncs,events accessible to other contracts

#[ink::contract]
pub mod erc20 {
    use ink_storage::collections::HashMap;

    #[ink(storage)]
    pub struct Erc20 {
        pub total_supply: Balance,
        pub balances: HashMap<AccountId, Balance>,
    }

    /// The ERC-20 error types
    #[derive(scale::Encode)]
    #[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
    pub enum Error {
        InsufficientBalance,
    }
    pub type Result<T> = core::result::Result<T, Error>;

    #[ink(event, anonymous)] // anon removes a lot of overhead with the event
    pub struct Transferred {
        #[ink(topic)]
        pub from: Option<AccountId>,
        #[ink(topic)]
        pub to: Option<AccountId>,
        #[ink(topic)]
        pub amount: Balance,
    }

    impl Erc20 {
        #[ink(constructor)]
        pub fn new(initial_supply: Balance) -> Self {
            let caller = Self::env().caller();
            let mut balances = HashMap::new();
            balances.insert(caller, initial_supply);
            Self::env().emit_event(Transferred {
                from: None,
                to: Some(caller),
                amount: initial_supply,
            });
            Self {
                total_supply: initial_supply,
                balances,
            }
        }
        // payable makes message payable: you can transfer tokens to the account of the contract
        // for instance to keep the contract alive (it can pay its fees)
        // selector: helps refactor contract func names without breaking the contract
        #[ink(message, payable, selector = "0xC0DECAFE")]
        pub fn total_supply(&self) -> Balance {
            self.total_supply
        }
        #[ink(message)]
        pub fn balance_of(&self, user: AccountId) -> Balance {
            self.balances.get(&user).copied().unwrap_or(0)
        }
        #[ink(message)]
        pub fn transfer(&mut self, to: AccountId, amount: Balance) -> Result<()> {
            let from = self.env().caller();
            let from_balance = self.balance_of(from);
            if from_balance < amount {
                return Err(Error::InsufficientBalance);
            }
            let to_balance = self.balance_of(to);
            self.balances.insert(from, from_balance - amount);
            self.balances.insert(to, to_balance + amount);
            self.env().emit_event(Transferred {
                from: Some(from),
                to: Some(to),
                amount,
            });
            Ok(())
        }
    }
}
