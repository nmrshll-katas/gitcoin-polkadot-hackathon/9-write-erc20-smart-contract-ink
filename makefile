.PHONY: all $(MAKECMDGOALS)
.DEFAULT_GOAL=dev
dev: deps clean node.build contract.build alice bob
	# ${node} --dev --tmp
ui: deps 
	# open https://polkadot.js.org/apps/#/settings?rpc=ws://127.0.0.1:9944
	cd web-ui && yarn start
	# open http://localhost:8000/substrate-front-end-template
bg=&> $(shell pwd)/.cache/proc/$@.out & echo $$$$ > $(shell pwd)/.cache/proc/$@.PID

# PROCESSES
alice: deps clean node.build
	${node} --base-path /tmp/alice --chain local --alice --port 30333 --ws-port 9944 --rpc-port 9933 --node-key 0000000000000000000000000000000000000000000000000000000000000001 --telemetry-url 'wss://telemetry.polkadot.io/submit/ 0' --validator    &> .cache/alice.out & echo $$! > .cache/alice.PID &
bob:
	${node} --base-path /tmp/bob --chain local --bob --port 30334 --ws-port 9945 --rpc-port 9934 --telemetry-url 'wss://telemetry.polkadot.io/submit/ 0' --validator --bootnodes /ip4/127.0.0.1/tcp/30333/p2p/12D3KooWEyoppNCUx8Yx66oV9fJnriXwCcXwDDUA2kj6vnc6iDEp    &> .cache/bob.out & echo $$! > .cache/bob.PID &
down:
	-ls -rt .cache/proc/*.PID | xargs cat | xargs -L 1 kill -9
	-rm .cache/proc/*.PID .cache/proc/*.out
# -kill `cat .cache/alice.PID` && rm .cache/alice.PID
# -kill `cat .cache/bob.PID` && rm .cache/bob.PID
termid=$(shell ps -o ppid= $(shell ps -o ppid= $$$$))


# COMMANDS
node.build:
	${?rebuild} && cd substrate-node-template/ && cargo check && \
	${global} cargo build --out-dir $(shell pwd)/.cache/bin -Z unstable-options
	echo ${code_sha} > .cache/build/code_sha
global=CARGO_TARGET_DIR=/tmp/.cargo/target-global
node=./.cache/bin/node-template
code_sha=$(shell echo $(shell find substrate-node-template -path "*target" -prune -type f -o -exec echo {} \; | xargs -I "{}" date -r {} "+%s") | $(shell [[ `uname`="Darwin" ]] &&echo "shasum -a 256" ||echo sha256sum) | cut -d" " -f1)
?rebuild=[[ ! -f .cache/build/code_sha ]] || [[ ${code_sha} != $(shell cat .cache/build/code_sha || echo _) ]] || (echo NO REBUILD && exit 0)
contract.build: deps
	# 'generate-metadata' also calls 'build'
	cd erc20 && cargo contract generate-metadata 
contract.deploy: deps contract.build
	# Alice is built-in dev account
	cd erc20 && cargo contract deploy --suri //Alice 
	# cd erc20 && cargo contract instantiate new true --code-hash CODE_HASH_FROM_CARGO_DEPLOY_OUTPUT --endowment 1000000000000 --suri //Alice
	# cd erc20 && cargo contract call get --rpc --contract CONTRACT_HASH_FROM_INSTANTIATE_OUTPUT --suri //Alice
	# cd erc20 && cargo contract call flip --contract HASH
front.build:
	cd web-ui && yarn install && yarn build


# DEPS
deps: installs
	@rustc --version | grep -E 'nightly.*2020-09-25' $s || rustup override set $v
	@rustup show | grep 'wasm32' $s || rustup target add wasm32-unknown-unknown --toolchain nightly
	@test -d substrate-node-template || (git clone -b v2.0.0 --depth 1 https://github.com/substrate-developer-hub/substrate-node-template && rm -rf ./substrate-node-template/.git)
	@test -d web-ui || (git clone https://github.com/substrate-developer-hub/substrate-front-end-template.git web-ui && rm -rf ./web-ui/.git)
	@mkdir -p .cache/keys .cache/build .cache/proc
installs:
	-@substrate --version $s || curl https://getsubstrate.io -sSf | bash -s -- --fast
	@rustc --version $s || rustup --version $s || curl https://sh.rustup.rs -sSf | sh -s -- -y; export PATH="$${HOME}/.cargo/bin:$${PATH}"
	@substrate --version | grep 2.0.0 $s || cargo +$v install node-cli --git https://github.com/paritytech/substrate.git --tag v2.0.0 --force --locked
	@subkey --version $s || cargo install --force subkey --git https://github.com/paritytech/substrate --version 2.0.0
	@cargo contract --version |grep 0.7.1 $s || cargo install cargo-contract --version 0.7.1 --features extrinsics
	@basic-http-server --version $s || cargo install basic-http-server
s = &>/dev/null
v = nightly-2020-09-26
clean: down
	-${node} purge-chain --base-path /tmp/alice --chain local -y
	-${node} purge-chain --base-path /tmp/bob --chain local -y
	-rm .cache/*.out
